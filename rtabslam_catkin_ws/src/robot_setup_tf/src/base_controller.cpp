#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <string>
#include <stdlib.h>

using namespace std;

void velCallback(const geometry_msgs::Twist::ConstPtr& vel){

  geometry_msgs::Twist new_vel = *vel;
  string nav_cmd;

  if(new_vel.linear.x != 0){
    if(new_vel.linear.x > 0)
      nav_cmd = "move_forward";
    else
      nav_cmd = "reverse";

    ROS_INFO("linear velocity: %.2f. -%s", new_vel.linear.x, nav_cmd.c_str());  

  }else if(new_vel.angular.z < -0.05 || new_vel.angular.z > 0.05){
    if(new_vel.angular.z < -0.1)//spin right
      nav_cmd = "spin_right";
    else
      nav_cmd = "spin_left";      

    ROS_INFO("angular velocity: %.2f. -%s", new_vel.angular.z, nav_cmd.c_str());

  }else{
    nav_cmd = "stationary";

    ROS_INFO("linear: %.2f,angular: %.2f -%s", new_vel.linear.x, new_vel.angular.z, nav_cmd.c_str()); 
  }
}

int main(int argc, char** argv){
  ros::init(argc, argv, "base_controller_listener");
  ros::NodeHandle n;

  ros::Subscriber sub = n.subscribe("mobile_base/commands/velocity",10,velCallback);

  ros::spin();

  return 0;
}
