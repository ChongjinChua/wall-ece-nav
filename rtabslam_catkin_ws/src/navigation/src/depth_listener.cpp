#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <string>
#include <iostream>
using namespace std;
bool stop = false;

void callback(const sensor_msgs::Image::ConstPtr& img){
  sensor_msgs::Image new_img = *img;
  int height_center = new_img.height / 2;
  int start = height_center * new_img.step;
  float sum = 0;
  int i;
  for(i = 0; i < new_img.step; i++){
    //sum += new_img.data[start];
    ROS_INFO("avg_sum: %i", new_img.data[start]);
    if(new_img.data[start++] < 85)
      stop = true;
  }
  ROS_INFO("----------------------------------");
  //sum /= new_img.step;

}

int main(int argc, char** argv){
  ros::init(argc, argv, "depth_listener");
  ros::NodeHandle n;
  ros::Subscriber sub = n.subscribe("camera/depth_registered/image_raw",1,callback);
  while(ros::ok() && !stop){
    ros::spinOnce();
  }

  return 0;
}
