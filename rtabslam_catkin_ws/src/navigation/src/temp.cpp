#include <ros/ros.h>
#include <ros/callback_queue.h>
#include <ros/callback_queue_interface.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <geometry_msgs/Twist.h>
#include <string>

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

bool goal_status = false;
int pose_arr[] = {1, 0, -1, 0};

void callback(const geometry_msgs::Twist::ConstPtr& vel){

  geometry_msgs::Twist new_vel = *vel;

  if(new_vel.angular.z == 0 && new_vel.linear.x == 0){
    goal_status = false;
    ROS_INFO("invalid goal pose, generating new pose..");    
  }else{
    goal_status = true;
    ROS_INFO("valid goal pose..");    
  }
}

int main(int argc, char** argv){

  ros::init(argc, argv, "base_controller");
  ros::NodeHandle n;
  ros::CallbackQueue my_q;
  n.subscribe("mobile_base/commands/velocity",1,callback);
  n.setCallbackQueue(&my_q);
  my_q.addCallback(callback);

  //ros::init(argc, argv, "simple_navigation_goals");
  //tell the action client that we want to spin a thread by default
  MoveBaseClient ac("move_base", true);

  //wait for the action server to come up
  while(!ac.waitForServer(ros::Duration(5.0))){
    ROS_INFO("Waiting for the mobile_base action server to come up");
  }

  move_base_msgs::MoveBaseGoal goal;

  //we'll send a goal to the robot to move 1 meter forward
  goal.target_pose.header.frame_id = "map";
  goal.target_pose.header.stamp = ros::Time::now();

  int x_ptr = 0;
  int y_ptr = 3;

  while(!goal_status){
    x_ptr %= 4;
    y_ptr %= 4;
    ROS_INFO("x=%i, y=%i",pose_arr[x_ptr],pose_arr[y_ptr]);
    goal.target_pose.pose.position.x = pose_arr[x_ptr++];
    goal.target_pose.pose.position.y = pose_arr[y_ptr++];
    goal.target_pose.pose.position.z = 0;    
    goal.target_pose.pose.orientation.x = 0.0;
    goal.target_pose.pose.orientation.y = 0.0;
    goal.target_pose.pose.orientation.z = 0.0;
    goal.target_pose.pose.orientation.w = 1.0;  

    ROS_INFO("Sending goal");
    ac.sendGoal(goal);

    ROS_INFO("Set goal pose's status");
    ros::CallbackQueue::CallOneResult r = my_q.callOne(ros::WallDuration(2.0));
    ROS_INFO("q status: %i", r);
  }

  //goal pose is valid
  ROS_INFO("Waiting for result...");
  ac.waitForResult();

  if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    ROS_INFO("Hooray, the base arrived at its destination");
  else
    ROS_INFO("The base failed to arrive at its destination for some reason");

  return 0;
}
