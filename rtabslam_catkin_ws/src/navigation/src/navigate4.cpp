#include <ros/ros.h>
#include <ros/callback_queue.h>
#include <ros/callback_queue_interface.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/Image.h>
#include <string>

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;
using namespace std;

bool path_clear;
int OBSTACLE_THRESH = 85;

void pc_callback(const sensor_msgs::Image::ConstPtr& img){
  sensor_msgs::Image new_img = *img;
  int height_center = new_img.height / 2;
  int start = height_center * new_img.step;
  float sum = 0;
  int i;
  for(i = 0; i < new_img.step; i++){
    if(new_img.data[start++] < OBSTACLE_THRESH)
      path_clear = true;
  }
}

class Controller
{
private:
  move_base_msgs::MoveBaseGoal goal;
  MoveBaseClient ac;
public:
  bool explore;

  Controller() : ac("move_base",true)
  {
    ROS_INFO("Waiting for action server to start.");
    ac.waitForServer();
    ROS_INFO("Action server started, sending goal.");
    explore = true;
  }

  void generate_goal(bool frame, float x, float y){
    if(frame)//base_link
      goal.target_pose.header.frame_id = "base_link";
    else//map
      goal.target_pose.header.frame_id = "map";
    goal.target_pose.header.stamp = ros::Time::now();

    ROS_INFO("s=%d, x=%f, y=%f",frame,x,y);
    goal.target_pose.pose.position.x = x;
    goal.target_pose.pose.position.y = y;
    goal.target_pose.pose.position.z = 0;    
    goal.target_pose.pose.orientation.x = 0.0;
    goal.target_pose.pose.orientation.y = 0.0;
    goal.target_pose.pose.orientation.z = 0.0;
    goal.target_pose.pose.orientation.w = 1.0;
  }  

  void send_goal(){
    ac.sendGoal(goal,
		boost::bind(&Controller::doneCb, this, _1),
		MoveBaseClient::SimpleActiveCallback(),
		MoveBaseClient::SimpleFeedbackCallback());
    ROS_INFO("Waiting for result...");
    ac.waitForResult(ros::Duration(10));
  }

  void clear_path(){
    path_clear = false;
    while(ros::ok() && !path_clear){
      ros::spinOnce();
      if(!path_clear)
	ROS_INFO("insert spin command here");  
    }
  }

  void move_and_find(){
    clear_path();
    generate_goal(true,0.5,0);
    send_goal();
    //find_object();
  }

  void doneCb(const actionlib::SimpleClientGoalState& state){
    ROS_INFO("Finished in state [%s]", state.toString().c_str());
    if (ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
      ROS_INFO("Hooray, the base arrived at its destination");
    }
    if (ac.getState() == actionlib::SimpleClientGoalState::ABORTED)
    {
      ROS_INFO("The base failed to arrive at its destination for some reason");
      ROS_INFO("Aborting exploration..");      
      explore = false;
    }          
  }
};

int main(int argc, char** argv){

  ros::init(argc, argv, "base_controller");
  ros::NodeHandle n;
  ros::Subscriber sub = n.subscribe("camera/depth_registered/image_raw",1,pc_callback);
  
  Controller ctrl;
  
  while(ctrl.explore){
    ctrl.move_and_find();
  }
  //return to base station
  return 0;
}


