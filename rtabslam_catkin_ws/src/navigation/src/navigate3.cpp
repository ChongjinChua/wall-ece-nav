#include <ros/ros.h>
#include <ros/callback_queue.h>
#include <ros/callback_queue_interface.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <geometry_msgs/Twist.h>
#include <string>

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;
using namespace std;

double pose_arr[] = {0.5,0,-0.5,0};

class Controller
{
private:
  int x_ptr;
  int y_ptr;  
  move_base_msgs::MoveBaseGoal goal;
  MoveBaseClient ac;
public:
  bool explore;

  Controller() : ac("move_base",true)
  {
    ROS_INFO("Waiting for action server to start.");
    ac.waitForServer();
    ROS_INFO("Action server started, sending goal.");
    explore = true;
  }

  void set_ptr(){
    x_ptr = 0;
    y_ptr = 3;
  }

  void generate_goal(){
    goal.target_pose.header.frame_id = "base_link";
    goal.target_pose.header.stamp = ros::Time::now();

    x_ptr %= 4;
    y_ptr %= 4;
    ROS_INFO("x=%f, y=%f",pose_arr[x_ptr],pose_arr[y_ptr]);
    goal.target_pose.pose.position.x = pose_arr[x_ptr++];
    goal.target_pose.pose.position.y = pose_arr[y_ptr++];
    goal.target_pose.pose.position.z = 0;    
    goal.target_pose.pose.orientation.x = 0.0;
    goal.target_pose.pose.orientation.y = 0.0;
    goal.target_pose.pose.orientation.z = 0.0;
    goal.target_pose.pose.orientation.w = 1.0;
  }

  void send_goal(){
    int goal_status = false;
    while(!goal_status){
      ROS_INFO("generating goal...");
      generate_goal();
      ROS_INFO("Sending goal...");
      ac.sendGoal(goal,
		  boost::bind(&Controller::doneCb, this, _1),
		  MoveBaseClient::SimpleActiveCallback(),
		  MoveBaseClient::SimpleFeedbackCallback());
      ROS_INFO("Waiting for result...");
      goal_status = ac.waitForResult(ros::Duration(10));
    }
  }

  void doneCb(const actionlib::SimpleClientGoalState& state){
    ROS_INFO("Finished in state [%s]", state.toString().c_str());
    if (ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
      ROS_INFO("Hooray, the base arrived at its destination");
      set_ptr();
    }
    if (ac.getState() == actionlib::SimpleClientGoalState::ABORTED)
    {
      ROS_INFO("The base failed to arrive at its destination for some reason");
      //ROS_INFO("Aborting..");      
      //explore = false;
    }          
  }
};

int main(int argc, char** argv){

  ros::init(argc, argv, "base_controller");
  Controller ctrl;
  ctrl.set_ptr();
  
  while(ctrl.explore){
    //if object not found:
    ctrl.send_goal();
    //else
    //  generate_goal_pose_of_target_object
    //  call arm movement sub_routine
  }
  return 0;
}
