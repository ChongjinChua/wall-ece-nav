(cl:in-package rtabmap_ros-msg)
(cl:export '(HEADER-VAL
          HEADER
          CAMERAINFO-VAL
          CAMERAINFO
          RGB-VAL
          RGB
          DEPTH-VAL
          DEPTH
          RGBCOMPRESSED-VAL
          RGBCOMPRESSED
          DEPTHCOMPRESSED-VAL
          DEPTHCOMPRESSED
))